# JS-Base-HomeWork6
Цикл forEach применяется для перебора всех элементов массива.Метод в параметре получает функцию, которая выполнится для каждого элемента массива. В эту функцию можно передавать 3 параметра.

Если эти параметры есть (они не обязательны), то в первый автоматически попадет элемент массива, во второй попадет его номер в массиве (индекс), а в третий - сам массив. 
Это более удобная реалзация перебора элементов массива, чем при помощи оператора for.